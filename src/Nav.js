import React from 'react'
import { Link } from 'react-router-dom'
import { sign_out } from './firebase-config'

export const Nav = () => (
  <>
    <nav>
      <Link className="btn" to='/calendar'>Calendar</Link>
      <Link className="btn" to='/profile'>Profile</Link>
      <Link className="btn" to='/settings'>Settings</Link>
      <button onClick={() => sign_out()}>Sign Out</button>
    </nav>
  </>
)