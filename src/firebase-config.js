import { initializeApp } from "firebase/app";
import { getFirestore } from '@firebase/firestore'
import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  signOut
} from 'firebase/auth'

  const firebaseConfig = {
    apiKey: "AIzaSyAlSyeGB11FeKAx_RQn0fPsLbtspavazGs",
    authDomain: "qalendar-339513.firebaseapp.com",
    projectId: "qalendar-339513",
    storageBucket: "qalendar-339513.appspot.com",
    messagingSenderId: "947673838159",
    appId: "1:947673838159:web:62413a62444591b3ef303a"
  };

  // this always needs to be exported first
  // Keep it between const provider & const firebaseConfig
  export const app = initializeApp(firebaseConfig)
  export const db = getFirestore(app)

  export const provider = new GoogleAuthProvider()
  export const auth = getAuth()

  export const sign_in = () => 
    signInWithPopup(auth, provider)
      .then((result) => {
        const at_quanterall_dot_com = result.user.email.includes("@quanterall.com")
        if(at_quanterall_dot_com) {
          const credential = GoogleAuthProvider.credentialFromResult(result)
          localStorage.setItem('@token', 'admin')
          return true
        } else {
          window.alert('Access denied')
          return false
        }
        // const user = result.user
      }).catch((error) => {
        console.log(error.code)
        return false
        // console.log(error.message)
        // console.log(error.email)
        // const credential = GoogleAuthProvider.credentialFromError(error)
      })

  export const sign_out = () => {
    signOut(auth)
      .then(() => {
        localStorage.removeItem("@token")
        window.location.reload()
      }).catch((error) => {
        console.log(error)
      })
  }
