import React from 'react'
import { Route, Redirect } from 'react-router-dom'

export const GuardedRoute = (props) => {
  const {component: Component, ...rest} = props
  const permission = (props) => {
    switch (localStorage.getItem("@token")) {
      case "admin":
        return <Component {...props}/>
      case "user":
        return <div>user :D</div>
      default: 
        return <Redirect to="/"/>
    }
  }
  return (
    <Route {...rest} render={(props) => permission(props)}/>
  )
}
