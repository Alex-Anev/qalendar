import {useState, useEffect} from 'react'
import {db} from '../firebase-config'
import {collection, getDocs, addDoc} from 'firebase/firestore'

export const CrudTest = () => {
  const [users, setUsers] = useState([])
  if(users !== []) console.log(users)
  const usersCollectionRef = collection(db, 'users')

  const [newFirstName, setNewFirstName] = useState(null)
  const [newLastName, setNewLastName] = useState(null)
  const [newPosition, setNewPosition] = useState(null)

  const createUser = async () => {
    console.log("asd")
    await addDoc(usersCollectionRef, {first_name: newFirstName, last_name: newLastName, position: newPosition})
  }

  useEffect(() => {
    const getUsers = async () => {
      const data = await getDocs(usersCollectionRef)
      setUsers(data.docs.map((doc) => ({...doc.data(), id: doc.id})))
      console.log(users)
    }
    getUsers()
  }, [])

  return (
    <div>
      <input placeholder="First Name" type="text" onChange={(e) => setNewFirstName(e.target.value)}/>
      <input placeholder="Last Name" type="text" onChange={(e) => setNewLastName(e.target.value)}/>
      <input placeholder="Position" type="text" onChange={(e) => setNewPosition(e.target.value)}/>
      <button onClick={() => {createUser()}}>Create User</button>

      {users.map((user) => {
        return <div>Name: {user.first_name} {user.last_name}</div>
      })}
    </div>
  )
}