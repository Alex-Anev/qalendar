import React, { useEffect, useState } from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interaction from '@fullcalendar/interaction'

import { db } from '../firebase-config'
import { collection, addDoc, getDocs } from 'firebase/firestore'

import { Modal } from 'react-responsive-modal'
import 'react-responsive-modal/styles.css'

import DateRangePicker from '@wojtekmaj/react-daterange-picker'

export const Calendar = () => {

  const [timestamps, setTimestamps] = useState(null)
  const [workWeekHolidays, setWorkWeekHolidays] = useState(null)

  useEffect(() => {
    // gets the hardcoded holidays from firebase
    const miscRef = collection(db, 'misc')
    const getHolidays = async () => {
      const data = await getDocs(miscRef)
      // TODO add a checker to pull the dates according to current year v
      setTimestamps(data.docs[0]._document.data.value.mapValue.fields[2022].arrayValue.values)
    }
    getHolidays()
  }, [])

  // NOTE helper for timestamp fixer & selectHandler(start, end)
  // need to convert the dates like this to have them in yyyy-mm-dd format
  // calendar library won't recognize the dates otherwise
  // https://stackoverflow.com/a/23593099/7295541
  const dateObjFormatter = (date) => {
    var 
      year = date.getFullYear(),
      month = '' + (date.getMonth() + 1),
      day = '' + date.getDate()
    if(month.length < 2) month = '0' + month
    if(day.length < 2) day = '0' + day
    return [year, month, day].join('-')
  }

  // turns the firebase timestamps from above into dates & 'fixes' weekend holidays
  if(timestamps !== null) {
    // TODO the foreach below can be done here v
    const dates = timestamps.map((ts) => { return new Date(ts.timestampValue) })
    const fixed_holidays = []
    dates.forEach((date) => {
      // TODO also check if the fixed holiday date doesn't fall on another holiday
      if(date.getDay() === 6) { // day 6 = saturday
        var t = new Date(date)
        fixed_holidays.push(new Date(t.setUTCDate(date.getUTCDate() + 2)))
      } else if (date.getDay() === 0) { // day 0 = sunday
        var t = new Date(date)
        fixed_holidays.push(new Date(t.setUTCDate(date.getUTCDate() + 1)))
      } else {
        fixed_holidays.push(date)
      }
    })
    
    var preppedForCalendar = fixed_holidays.map((date) => {
      return {
        start: dateObjFormatter(date),
        // TODO do this better
        // every object has to specify the display type & color
        // find a way to set this for all objects automatically
        display: 'background',
        color: 'red'
      }
    })
    setWorkWeekHolidays(preppedForCalendar)
    setTimestamps(null) // NOTE not doing this causes re-renders
  }

  const [datesOfRequest, setDatesOfRequest] = useState(null)

  const selectHandler = (start, end) => {
    const totalDaysFromRequest = Math.round((end.getTime() - start.getTime()) / (1000 * 60 * 60 * 24))

    // NOTE this makes an array of all selected dates
    // between start date & end date
    var 
      arr = new Array(),
      dt = new Date(start)
    while(dt <= end) {
      arr.push(new Date(dt))
      dt.setDate(dt.getDate() + 1)
    }
  
    // NOTE count how many of the requested days are a weekend
    let weekendCounter = 0
    arr.slice(0, -1).forEach(d => (d.getDay() === 6 || d.getDay() === 0) && weekendCounter++ )

    // NOTE prepare requested days & holidays for comparison
    const datesRequested = arr.map(d => dateObjFormatter(d)).slice(0, -1)
    const simpleWorkWeeHolidays = workWeekHolidays.map(d => d.start)

    // NOTE count matches between requested days & holidays
    let matchCounter = 0
    datesRequested.forEach(date => simpleWorkWeeHolidays.includes(date) && matchCounter++ )

    // NOTE this object is used for the request form later
    const validRequestedDays = totalDaysFromRequest - (weekendCounter + matchCounter)
    setDatesOfRequest({
      start: start, // TODO change accordingly if invalid date
      end: end, // TODO change accordingly if invalid date
      totalDaysRequested: validRequestedDays
    })
    // TODO the way this is done is ugly. I have 2 seperate states
    // for 1 set of values (start date & end date)
    setDatePickerValue([start, new Date(end.setDate(end.getDate() - 1))])
    onOpenModal()
  }

  const [open, setOpen] = useState(false)
  const onOpenModal = () => setOpen(true)
  const onCloseModal = () => setOpen(false)

  const [datePickerValue, setDatePickerValue] = useState([null, null])

  const [name, setName] = useState(localStorage.getItem('@name'))
  const [position, setPosition] = useState(null)
  const [leaveType, setLeaveType] = useState('paid')
  
  const handleSubmit = async (event) => {
    event.preventDefault()
    const requestsCollectionRef = collection(db, 'requests')
    await addDoc(requestsCollectionRef, {
      name: name,
      position: position,
      leaveType: leaveType,
      start: datePickerValue[0],
      end: datePickerValue[1],
      freeDays: datesOfRequest.totalDaysRequested,
      submissionDate: new Date()
    })
  }

  return (
    <>
      <FullCalendar
        firstDay={1}
        plugins={[ dayGridPlugin, interaction ]}
        selectable={true}
        initialView="dayGridMonth"
        contentHeight={window.screen.availHeight}
        events={ workWeekHolidays }
        select={d => selectHandler(d.start, d.end)}
        // NOTE this could give me the same troubles I had above with date formatting
        // where the day would get changed by toISOString() because of timezones & etc
        selectConstraint={{start: new Date().toISOString().split('T')[0]}}
      />

      <Modal
        open={open}
        onClose={onCloseModal}
        center
        closeIcon={null}
      >
        {/* i know and idc, i'm trying to make this work ASAP */}
        {(datesOfRequest !== null) && (
          <div>
            <h2>Request Form</h2><br/>
            <form onSubmit={handleSubmit}>
              <input type={'text'} defaultValue={name} onChange={setName}/><br/>
              <input type={'text'} placeholder={'your position'} onChange={(e) => setPosition(e.target.value)}/> <br/>
              <select defaultValue={'paid'} onChange={(e) => setLeaveType(e.target.value)}>
                <option value={'paid'}>Paid</option>
                <option value={'unpaid'}>Unpaid</option>
                <option value={'additional paid'}>Additional paid</option>
              </select><br/>
              <DateRangePicker
                onChange={setDatePickerValue}
                value={datePickerValue}
                openCalendarOnFocus={false}
                format={'dd-MM-yyyy'}
              />
              <br/>
              Total Days Requested: {datesOfRequest.totalDaysRequested} <br/>
              Date of Request: {new Date().toISOString().split('T')[0]}<br/>
              <input type={'submit'} value={'Next'}/>
            </form>
          </div>
        )}
      </Modal>
    </>
  )
}

