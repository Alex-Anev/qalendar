import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import { Redirect } from 'react-router-dom'
import { signInWithPopup, GoogleAuthProvider } from 'firebase/auth'
import { auth, provider } from '../firebase-config'

export const Login = () => {
  const [token, setToken] = useState(null)

  const sign_in = () => {
    signInWithPopup(auth, provider)
      .then(async (result) => {
        const at_quanterall_dot_com = result.user.email.includes("@quanterall.com")
        if(at_quanterall_dot_com) {
          const credential = GoogleAuthProvider.credentialFromResult(result)
          await localStorage.setItem('@token', 'admin')
          await localStorage.setItem('@name', result.user.displayName)
          setToken('admin')
        } else {
          window.alert('Access denied')
        }
        // const user = result.user
      }).catch((error) => {
        console.log(error.code)
        // console.log(error.message)
        // console.log(error.email)
        // const credential = GoogleAuthProvider.credentialFromError(error)
      })
    }

  return (
    <>
      <Button onClick={() => sign_in()}>Sign In with Google</Button>
      {token && <Redirect to="/calendar" />}
    </>
  )
}
