import React, { useEffect, useState } from 'react'
import { Nav } from '../Nav'

export const Layout = ({children}) => {
  // const [tokenChecker, setTokenChecker] = useState(false)
  // useEffect(() => {setTokenChecker(!tokenChecker)}, StorageEvent.newValue)
  return (
    <div>
       {localStorage.getItem("@token") ? <Nav/> : null} 
       {/* this doesn't work the way it should */}
      <main>
        {children}
      </main>
    </div>
  )
}