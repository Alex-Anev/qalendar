import React from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import { Calendar } from './pages/Calendar'
import { Profile } from './pages/Profile'
import { Settings } from './pages/Settings'
import { Login } from './pages/Login'

import { Layout } from './utils/Layout'
import { GuardedRoute } from './components/GuardedRoute'

const App = () => {
  return (
    <>
      <Router>
        <Layout>
          <Switch>
            <Route exact path="/" component={Login}/>
            <GuardedRoute path="/calendar" component={Calendar}/>
            <GuardedRoute path="/profile" component={Profile}/>
            <GuardedRoute path="/settings" component={Settings}/>
          </Switch>
        </Layout>
      </Router>
    </>
  )
}

export default App;
